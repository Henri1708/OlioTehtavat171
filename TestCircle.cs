﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circle
{
    public class TestCircle
    {  
        public static void Main(String[] args)
        {
            Circle c1 = new Circle(5.0);
            Console.WriteLine(c1.toString());

            Circle c2 = new Circle(2.0);
            Console.WriteLine(("The circle has radius of "
               + c2.getRadius() + " and area of " + c2.getArea() + "and is the color of " + c2.getColor()));

            Circle c3 = new Circle(3.0);
            Console.WriteLine(("The circle has radius of "
               + c3.getRadius() + " and area of " + c3.getArea() + "and is the color of " + c3.getColor()));

            Circle c4 = new Circle(4.0);
            c4.setRadius(5.0);
            Console.WriteLine("radius is: " + c4.getRadius());
            c4.setColor("blue");
            Console.WriteLine("color is: " + c4.getColor());
          
        }
    }
}
