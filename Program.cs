﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circle
{
    public class Circle
    {
        private double radius;
        private String color;


       // public Circle()
       public String toString()
        {
            return "Circle[radius=" + radius + " color=" + color + "]";
        //    radius = 1.0;
        //    color = "red";
        }

        public Circle(double radius)
        {
            this.radius = radius;

            radius = radius;
            color = "red";
        }

        public Circle(double radius, String color)
        {
            this.radius = radius;
            this.color = color;

            radius = radius;
            color = color;
        }

        public double getRadius()
        {
            return radius;
        }

        public double getArea()
        {
            return radius * radius * Math.PI;
        }

        public String getColor()
        {
            return color; 
        }

        public void setRadius(double newRadius)
        {
            radius = newRadius;
        }

        public void setColor(String newColor)
        {
            color = newColor;
        }

    }
}